/* ###########################################
 * #  Minimal SDL2 + OpenGL3 ImGui Tutorial  #
 * #  Author: Gabriel Morales                #
 * #  Date: 2019-08-23                       #
 * ###########################################
 *
 *  A simple example of how to create and use
 *  ImGui using SDL2 and OpenGL 3.2.
 *
 * DEPS:
 * -----------------------------------------
 * libsdl2-dev
 * gl3w (build from source)
 * libglm-dev
 *
 * COMPILE:
 * -----------------------------------------
 * SOURCES = main.cpp
 * SOURCES += ../imgui_impl_sdl.cpp ../imgui_impl_opengl3.cpp
 * SOURCES += ../../imgui.cpp ../../imgui_demo.cpp ../../imgui_draw.cpp ../../imgui_widgets.cpp
 * OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))
 * UNAME_S := $(shell uname -s)
 * CXXFLAGS = -I../ -I../../
 * CXXFLAGS += -g -Wall -Wformat
 * SOURCES += ../libs/gl3w/GL/gl3w.c
 * CXXFLAGS += -I../libs/gl3w
 *
 * LICENSE:
 * -----------------------------------------
 * This file is in the public domain; it is free
 * use in any manner one wants. This code may
 * be used under the terms of the
 * Creative Commons CC0 license, either ver 1.0
 * or (at your option) any later version.
 * details: http://creativecommons.org/publicdomain/zero/1.0/
 * This software distributed without any warranty.
 *
 * BIBLOGRAPHY:
 * -----------------------------------------
 * https://github.com/ocornut/imgui <- ImGui source
 * https://gist.github.com/koute/7391344 <- Minimal SDL2 + OpenGL3 Skeleton
 * https://github.com/skaslev/gl3w <- gl3w extension handler source
 */

#define GL_GLEXT_PROTOTYPES                     //Setup Prototypes for GL Extensions
#define IMGUI_DEFINE_PLACEMENT_NEW              //Define a new placement for ImGui
#define IMGUI_DEFINE_MATH_OPERATORS             //create math operators for ImGUI vector addition, multiplication, etc.

#include <stdio.h>

#include "imgui.h"                              //ImGUI header
#include "imgui_impl_sdl.h"                     //ImGUI implement SDL2
#include "imgui_impl_opengl3.h"                 //ImGUI implement OpenGL3

#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    #define IMGUI_GL_EXTENSION_LOADER \
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    #define IMGUI_GL_EXTENSION_LOADER \
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    #define IMGUI_GL_EXTENSION_LOADER \
    bool err = gladLoadGL() == 0;
#else
    #define IMGUI_GL_EXTENSION_LOADER \
    bool err = false;
#endif

#include <GL/gl3w.h>                            //OpenGL Extension Wrangler
#include <glm/glm.hpp>                          //gl math library
#include <glm/gtc/matrix_transform.hpp>         //gl matrix transformations
#include <glm/gtc/type_ptr.hpp>                 //gl math pointers

#include <SDL2/SDL.h>                           //SDL Header
#include <SDL2/SDL_opengl.h>                    //SDL implement OpenGL

//---Window state---
static const int Window_Width = 800;               //Window Width
static const int Window_Height = 600;              //Window Height
static bool Loop_Done = false;
//---ImGUI state---
static bool show_demo_window = true;
static bool show_another_window = false;
ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
//---OpenGL state---
static GLuint vs, fs, program;                                 //pointers to our Vertex Shader, Fragment Shader, and OpenGL Shader Program
static GLint status;                                           //OpenGL Status indicator
static GLuint vao, vbo;                                        //OpenGL VertexArrayObject, VertexBufferObject
static glm::mat4 Projection_Matrix;
//---SDL state---
SDL_Window *window;
SDL_GLContext context;
static SDL_Event event;

#define GLSL_VERSION "#version 150"
#define NUMBER_OF_TRIANGLES 2
#define NUMBER_OF_VERTEX NUMBER_OF_TRIANGLES*3

//***********************************
//vertex_shader:
//source code to  our vertex shader
//***********************************
static const char * vertex_shader =
    GLSL_VERSION "\n"
    "in vec2 i_position;\n"
    "in vec4 i_color;\n"
    "out vec4 v_color;\n"
    "uniform mat4 u_projection_matrix;\n"
    "void main() {\n"
    "    v_color = i_color;\n"
    "    gl_Position = u_projection_matrix * vec4( i_position, 0.0, 1.0 );\n"
    "}\n";

//***********************************
//fragment_shader:
//source code to  our fragment shader
//***********************************
static const char *fragment_shader =
    GLSL_VERSION "\n"
    "in vec4 v_color;\n"
    "out vec4 o_color;\n"
    "void main() {\n"
    "    o_color = v_color;\n"
    "}\n";

//***********************************
//t_attrib_id:
//a list, in order, of the attributes
//our shaders use in their source codes
//***********************************
typedef enum t_attrib_id
{
    attrib_position,
    attrib_color
} t_attrib_id;

//***********************************
//int SetupShaderProgram():
//compiles vertex shader,
//compiles fragment shader,
//then links them both into OpenGL
//shader program named "program"
//defines attribute lables named
//"i_position" and "i_color" in sources
//***********************************
int SetupShaderProgram()
{
    vs = glCreateShader( GL_VERTEX_SHADER );                //create vertex shader and have vs point to it
    fs = glCreateShader( GL_FRAGMENT_SHADER );              //create fragment shader and have fs point to it

    //---compile vertex shader---
    int length = strlen( vertex_shader );                                   //buffer to hold vertex shader source length
    glShaderSource( vs, 1, ( const GLchar ** )&vertex_shader, &length );    //Prep Vertex Shader Source for OpenGL to compile
    glCompileShader( vs );                                                  //compile Vertex Shader Source


    glGetShaderiv( vs, GL_COMPILE_STATUS, &status );                        //Check if Vertex shader compiled correctly
    if( status == GL_FALSE )
    {
        fprintf( stderr, "vertex shader compilation failed\n" );            //error - didn't compile correctly
        return 1;
    }

    //---compile fragment shader---
    length = strlen( fragment_shader );                                     //buffer to hold fragment shader source length
    glShaderSource( fs, 1, ( const GLchar ** )&fragment_shader, &length );  //prep Fragment Shader Source for OpenGL to compile
    glCompileShader( fs );                                                  //compile Fragment Shader Source

    glGetShaderiv( fs, GL_COMPILE_STATUS, &status );                        //check if Fragment Shader compiled correctly
    if( status == GL_FALSE )
    {
        fprintf( stderr, "fragment shader compilation failed\n" );          //error - fragment shader didn't compile correctly
        return 1;
    }

    program = glCreateProgram();                                            //create OpenGL shader program, program points to it
    glAttachShader( program, vs );                                          //attach vertex shader to our OpenGL shader
    glAttachShader( program, fs );                                          //attach fragment shader to our OpenGL shader

    glBindAttribLocation( program, attrib_position, "i_position" );         //define attribute label in OpenGL shader: "i_position"
    glBindAttribLocation( program, attrib_color, "i_color" );               //define attribute label in OpenGL shader: "i_color"
    glLinkProgram( program );                                               //link OpenGL shader program

    glUseProgram( program );                                                //set OpenGL shader program as active

    return 1;
}

//***********************************
//int SetupIMGUI():
//Accounts for which GL Extension Wrangler
//we are using, then sets up ImGUI.
//also sets up ImGUI styles.
//Calls SDL2 and OpenGL3 specific
//ImGUI implementations, because
//those are the APIs we are using.
//***********************************
int SetupIMGUI()
{
    // Setup Dear ImGui context
    IMGUI_GL_EXTENSION_LOADER;
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, context);
    ImGui_ImplOpenGL3_Init(GLSL_VERSION);

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'misc/fonts/README.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    return 1;
}

//***********************************
//int SetupVAO():
//Creates a Vertex array object,
//and a vertex buffer object to
//put into the VAO. Defines attributes
//inside of VBO, allocates RAM for them
//then stuffs them full of data
//***********************************

int SetupVAO()
{
    glGenVertexArrays( 1, &vao );                               //generate a VAO
    glGenBuffers( 1, &vbo );                                    //generate a VBO
    glBindVertexArray( vao );                                   //Set "vao" as working vertex array
    glBindBuffer( GL_ARRAY_BUFFER, vbo );                       //set "vbo" as working vertex buffer

    glEnableVertexAttribArray( attrib_position );               //enable attribute "position" in VBO
    glEnableVertexAttribArray( attrib_color );                  //enable attribute "color" in VBO

    glVertexAttribPointer( attrib_color, 4, GL_FLOAT, GL_FALSE, sizeof( float ) * NUMBER_OF_VERTEX, 0 );                                   //define attribute "color" as 4 floats, for all vertexes
    glVertexAttribPointer( attrib_position, 2, GL_FLOAT, GL_FALSE, sizeof( float ) * NUMBER_OF_VERTEX, ( void * )(4 * sizeof(float)) );    //define attribute "position" as 2 floats, for all vertexes


    //Our vertex data:
    //This is split into 2 groups, each represents a triangle and each is 3 vertexes big,
    //each vertex is 6 floats big (4 floats "color", 2 floats "position")
    const GLfloat g_vertex_buffer_data[] = {
    /*TRIANGLE 1*/
    /*  R, G, B, A, X, Y  */
        1, 0, 0, 1, 0, 0,
        0, 1, 0, 1, Window_Width, 0,
        0, 0, 1, 1, Window_Width, Window_Height,
    /*TRIANGLE 2*/
    /*  R, G, B, A, X, Y  */
        1, 0, 0, 1, 0, 0,
        0, 0, 1, 1, Window_Width, Window_Height,
        1, 1, 1, 1, 0, Window_Height
    };

    glBufferData( GL_ARRAY_BUFFER, sizeof( g_vertex_buffer_data ), g_vertex_buffer_data, GL_STATIC_DRAW );      //send vertex data to VBO we are working on
    glDisable( GL_DEPTH_TEST );

    return 1;
}

//***********************************
//int SetupSDLWindow():
//Initializes SDL, then creates a
//SDL Window with various settings.
//Also creates an OpenGL context
//within SDL Window, and sets
//the OpenGL version number (3.2)
//***********************************
int SetupSDLWindow()
{
    SDL_Init( SDL_INIT_VIDEO );                                 //start video
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );              //turn on double buffering
    SDL_GL_SetAttribute( SDL_GL_ACCELERATED_VISUAL, 1 );        //turn on hardware video acceleration
    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );                  //set 32-bit (R,G,B,A) color
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );                             //set OpenGL version 3.2
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

    //create our window, then create an OpenGL context in our window
    window = SDL_CreateWindow( "OUR TOOL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Window_Width, Window_Height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    context = SDL_GL_CreateContext( window );

    return 1;
}

//***********************************
//int SetupGLViewport():
//Clears the background to a certain
//color, then sets the viewport
//according to screen width/height.
//Then calculates the projection matrix
//using GLM.
//***********************************
int SetupGLViewport()
{
    //set the background color
    glClearColor( 0.5, 0.0, 0.0, 0.0 );
    //set the viewport dimensions
    glViewport( 0, 0, Window_Width, Window_Height );
    //set and use orthoscopic (2D) projection in world coordinates
    /*                             left    right      top      bottom      near   far     */
    Projection_Matrix = glm::ortho(0.0f,(float)Window_Width, 0.0f, (float)Window_Height, 0.0f, 100.0f); //

    return 1;
}

//***********************************
//int DrawIMGUI():
//creates a new ImGui_OpenGL3 frame
//within the SDLWindow. Then runs
//a series of demos
//***********************************
int DrawIMGUI()
{
    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(window);
    ImGui::NewFrame();

    // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
    if (show_demo_window)
        ImGui::ShowDemoWindow(&show_demo_window);

    // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
    {
        static float f = 0.0f;
        static int counter = 0;

        ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

        ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
        ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
        ImGui::Checkbox("Another Window", &show_another_window);

        ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
        ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

        if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
            counter++;
        ImGui::SameLine();
        ImGui::Text("counter = %d", counter);

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::End();
    }

    // 3. Show another simple window.
    if (show_another_window)
    {
        ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
        ImGui::Text("Hello from another window!");
        if (ImGui::Button("Close Me"))
            show_another_window = false;
        ImGui::End();
    }
}

//***********************************
//int HandleEvents():
//Polls SDL for input events,
//and handles them
//***********************************
int HandleEvents()
{
    while (SDL_PollEvent(&event))
    {
        ImGui_ImplSDL2_ProcessEvent(&event);
        if (event.type == SDL_QUIT)
            Loop_Done = true;
        if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
            Loop_Done = true;
        switch( event.type )
        {
            case SDL_KEYUP:
                if( event.key.keysym.sym == SDLK_ESCAPE )
                    Loop_Done = true;
            break;
        }
    }

    return 1;
}

//***********************************
//int Framework_Loop():
//Main loop of the program
//"Loop_Done" is gate to break loop
//***********************************
int Framework_Loop()
{
    while (!Loop_Done)
    {
        glClear( GL_COLOR_BUFFER_BIT );

        HandleEvents();

        //---Draw OpenGL---
        glBindVertexArray( vao );                                                                                                            //use VAO to draw with
        glUniformMatrix4fv( glGetUniformLocation( program, "u_projection_matrix" ), 1, GL_FALSE, glm::value_ptr(Projection_Matrix) );        //set uniform attribute "projection matrix"
        glDrawArrays( GL_TRIANGLES, 0, NUMBER_OF_VERTEX );                                                                                   //command OpenGL to draw vao

        //---Draw ImGUI---
        DrawIMGUI();

        //---Rendering---
        ImGui::Render();                                                //render ImGUI
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        SDL_GL_SwapWindow( window );                                    //tells SDL to swap the framebuffer so it'll display our new frame
    }
    return 1;
}

//***********************************
//int Quit():
//Shuts down SDL subsystems
//***********************************
int Quit()
{
    SDL_GL_DeleteContext( context );
    SDL_DestroyWindow( window );
    SDL_Quit();

    return 1;
}

//***********************************
//int main(arg count, args):
//Main program vector
//***********************************
int main( int argc, char * argv[] )
{
    SetupSDLWindow();
    SetupIMGUI();
    SetupShaderProgram();
    SetupVAO();
    SetupGLViewport();

    Framework_Loop();

    Quit();
    return 0;
}
